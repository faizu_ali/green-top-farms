class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.string :name
      t.datetime :runned_at

      t.timestamps
    end
  end
end
