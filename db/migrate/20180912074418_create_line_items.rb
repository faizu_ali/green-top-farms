class CreateLineItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.string :name
      t.decimal :price
      t.integer :quantity
      t.integer :product_id, limit: 8
      t.integer :variant_id, limit: 8
      t.date :delivery_date
      t.datetime :delivery_time
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
