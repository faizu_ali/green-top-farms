class AddStartEndTimeColumnToOrderItem < ActiveRecord::Migration[5.2]
  def change
    add_column :order_items, :start_time, :time
    add_column :order_items, :end_time, :time
  end
end
