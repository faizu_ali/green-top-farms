class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :name
      t.decimal :price
      t.string :customer_name
      t.jsonb :details, default: '{}'
      t.timestamps
    end
    add_index  :orders, :details, using: :gin
  end
end
