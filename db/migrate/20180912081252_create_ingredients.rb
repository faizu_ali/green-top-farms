class CreateIngredients < ActiveRecord::Migration[5.2]
  def change
    create_table :ingredients do |t|
      t.string :name
      t.string :unit
      t.decimal :quantity
      t.references :order_item, foreign_key: true

      t.timestamps
    end
  end
end
