Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'application#index'
  resources :orders
  resources :ingredients
  resources :users
  resources :order_items do
    post :print
    post :print_sticker
    collection do
      post :multiple_prints
    end
  end

  require 'sidekiq/web'
  require 'sidekiq/cron/web'

  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
  end

end
