json.array! @order_items do |order_item|
  puts order_item.as_json
  json.order_item_name order_item.name
  json.order_name order_item.order.name
  json.company_name order_item.order.company_name
  json.customer_name order_item.order.customer_name
  json.order_item_quantity order_item.quantity
  json.ingredients_count order_item.ingredients.count
  json.ingredients order_item.ingredients do |ingredient|
    json.name ingredient.name
    json.quantity ingredient.quantity
  end
end