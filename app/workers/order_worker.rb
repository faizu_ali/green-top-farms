require 'rubygems'
require 'shopify_api'

class OrderWorker

  JOB_NAME = "OrderJob"

  include Sidekiq::Worker
  sidekiq_options :retry => 5

  def perform
    last_runned_at = Job.where(name: JOB_NAME).order(created_at: :desc).first.runned_at.utc.to_time.iso8601 rescue (Time.now - 1.month).utc.to_time.iso8601
    Shopify.new
    page=1
    loop do
      orders =  ShopifyAPI::Order.where(created_at_min: last_runned_at, limit: 250, page: page )

      orders.each do |order|
        new_order = Order.new_order_from_hash(order)
        unless Order.find_by_name(new_order.name).present?
          new_order.save
        end
      end
      orders.count == 0 ? break : page+=1
    end
    Job.create!(name: JOB_NAME ,runned_at: Time.now)
  end

end
