class IngredientsController < ApplicationController

  def index
    @q = Ingredient.ransack(params[:q])
    @ingredients = @q.result.paginate(page: params[:page], per_page: 30)
  end

end
