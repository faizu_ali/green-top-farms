class OrderItemsController < ApplicationController
  before_action :find_order_item, only: [:print, :show, :print_sticker, :multiple_print]

  def index
    @q = OrderItem.joins(:order).ransack(params[:q])
    @order_items = @q.result
    @search_delivery_date = ''
    @search_created_date = ''
    if params[:search]
      if params[:search][:delivery_date] and params[:search][:delivery_date] != ''
        @search_delivery_date =  params[:search][:delivery_date]
        delivery_start, delivery_end = params[:search][:delivery_date].split(' - ')
        @order_items = @order_items.having_delivery_date_between(Date.strptime(delivery_start, '%m/%d/%Y'), Date.strptime(delivery_end, '%m/%d/%Y'))
      end

      if params[:search][:created_at] and params[:search][:created_at] != ''
        @search_created_date = params[:search][:created_at]
        created_start, created_end = params[:search][:created_at].split(' - ')
        @order_items = @order_items.where(orders: {created_at: DateTime.strptime(created_start, '%m/%d/%Y')..DateTime.strptime(created_end, '%m/%d/%Y')})
      end
    end
    @order_items = @order_items.paginate(page: params[:page], per_page: 30).decorate
  end

  def show
    @order = @order_item.order.decorate
  end

  def print

  end

  def print_sticker

  end

  def multiple_prints
    @order_items = OrderItem.where(id: params[:order_item_ids]).decorate || []
  end

  private

  def find_order_item
    @order_item = OrderItem.find_by_id(params[:id] || params[:order_item_id]).decorate
  end
end
