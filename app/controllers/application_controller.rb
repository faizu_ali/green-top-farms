class ApplicationController < ActionController::Base

  before_action :authenticate_user!
  layout :layout_by_resource

  def index
    @search_delivery_date = (params[:search] and params[:search][:delivery_date] and params[:search][:delivery_date] != '' ) ? params[:search][:delivery_date] : "#{Date.today.beginning_of_month.strftime('%m/%d/%Y')} - #{Date.today.end_of_month.strftime('%m/%d/%Y')}"
    delivery_start, delivery_end = @search_delivery_date.split(' - ')
    @ingredients = Ingredient.deliver_quantity_in_date_between(Date.strptime(delivery_start, '%m/%d/%Y'), Date.strptime(delivery_end, '%m/%d/%Y'))
  end

  private

  def layout_by_resource
    if devise_controller?
      "devise"
    else
      "application"
    end
  end
end
