class OrdersController < ApplicationController
  before_action :authenticate_user!, except:[:create]

  skip_before_action :verify_authenticity_token ,only: [:create]

  include ShopifyWebhook


  def show
    @order = Order.find_by_id(params[:id]).decorate
  end

  # POST /orders (as webhooks from fambrand stores)
  def create
    request.body.rewind
    data = request.body.read
    verified = verify_webhook(data, request)
    if verified
      @order = Order.new_order_from_json(data)
      if @order.present?
        @order.save
        # @order.delay(:retry => false).save
        head :ok
      end
    else
      head :error
    end
  end

end
