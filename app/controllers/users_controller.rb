class UsersController < ApplicationController

  before_action :admin?

  def index
    @q = User.ransack(params[:q])
    @users = @q.result.order(created_at: :desc).paginate(page: params[:page], per_page: 30)
  end

  def new
    @user = User.new
  end
  def create
    @user = User.new user_params
    if @user.save!
      flash[:success] = "User Successfully created."
    else
      flash[:errors] = @user.errors.full_messages
    end

    redirect_to users_path
  end

  private

  def admin?
    redirect_to root_url, notice: "You don't have permission to access this page." if not current_user.admin?
  end

  def user_params
    params.require(:user).permit(:email, :password)
  end

end
