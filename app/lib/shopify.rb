require 'rubygems'
require 'shopify_api'

class Shopify

  def initialize()
    shop_url = "https://#{ENV["api_key"]}:#{ENV["password"]}@#{ENV["shop_name"]}.myshopify.com/admin"
    ShopifyAPI::Base.site = shop_url
  end

end