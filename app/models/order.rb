class Order < ApplicationRecord
  after_create :process_order
  has_many :order_items
  has_many :ingredients, through: :order_items


  store_accessor :details, :total_price, :customer, :line_items, :tags, :billing_address

  def self.new_order_from_json(order_json)
    order = JSON.parse(order_json)
    new(name: order['name'],
        price: order['total_price'],
        customer_name: order['customer']['first_name']+ " " +order['customer']['last_name'],
        details: order,
    )
  end

  def self.new_order_from_hash(order_hash)
    new(name: order_hash.name,
        price: order_hash.total_price,
        customer_name: ((order_hash.customer.first_name+ " " +order_hash.customer.last_name) rescue ""),
        details: order_hash,
    )
  end

  def process_order
    self.line_items.each do |line_item|
      order_item = self.order_items.new(
          name: line_item['name'],
          price: line_item['price'],
          quantity: line_item['quantity'],
          product_id: line_item['product_id'],
          variant_id: line_item['variant_id'],
          delivery_date: (
          Date::strptime(line_item['properties'].select{ |property| property['name'] == 'Date' }.first['value'], "%m/%d/%y") rescue nil
          )
          # delivery_time: line_item['properties'].select{ |property| property['name'].gsub(/\W/,"") == 'Time' }.first['value']
      )

      _time = line_item['properties'].select{ |property| property['name'] == 'Time' }.first['value'] rescue nil
      if not _time.nil?
        order_item.start_time, order_item.end_time = _time.split('-')
      end
      order_item.save!
      line_item['properties'].each do |property|
        unless ["Date","Time"].include? property['name']
          quantity , unit = property['value'].split(' ')
          order_item.ingredients.create!(name: property['name'], quantity: quantity.to_f, unit: unit)
        end
      end
    end

  end

end
