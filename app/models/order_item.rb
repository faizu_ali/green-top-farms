class OrderItem < ApplicationRecord
  has_many :ingredients
  belongs_to :order

  scope :having_delivery_date_between, ->(start_date, end_date) { where(delivery_date: start_date..end_date) }

end
