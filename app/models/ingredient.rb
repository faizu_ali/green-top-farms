class Ingredient < ApplicationRecord
  belongs_to :order_item

  scope :deliver_quantity_in_date_between, ->(start_date, end_date) { joins(:order_item).select("ingredients.unit as ingredient_unit, ingredients.name as ingredient_name, SUM(ingredients.quantity * order_items.quantity) as ingredient_quantity").where(order_items: {delivery_date: start_date..end_date}).group('ingredient_name, ingredient_unit').order('ingredient_name') }
end
