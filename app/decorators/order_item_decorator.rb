class OrderItemDecorator < Draper::Decorator
  delegate_all
  decorates_association :order
  def self.collection_decorator_class
    PaginatingDecorator
  end
  def formated_delivrey_date
    object.delivery_date.nil? ? ' --' : object.delivery_date.strftime('%b %d, %Y')
  end

  def formated_delivery_time
    object.start_time.strftime("%I:%M%p").to_s + "-" + object.end_time.strftime("%I:%M%p").to_s rescue " -- "
  end
end
