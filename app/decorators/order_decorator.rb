class OrderDecorator < Draper::Decorator
  delegate_all


  ['note', 'email', 'shipping_address', 'contact_email', 'total_price_usd', 'phone'].each do |_method_name|
    define_method "#{_method_name}" do
      details[_method_name]
    end
  end

  def customer_shipping_address
    "#{shipping_address}"
  end

  def shopify_order_id
    details['id']
  end

  def company_name
    object.billing_address['company'] || '--'
  end
end
