//----------------------------------------------------------------------------
//
//  $Id: PrintLabel.js 12098 2010-06-01 22:27:03Z vbuzuev $
//
// Project -------------------------------------------------------------------
//
//  DYMO Label Framework
//
// Content -------------------------------------------------------------------
//
//  DYMO Label Framework JavaScript Library Samples: Print label
//
//----------------------------------------------------------------------------
//
//  Copyright (c), 2010, Sanford, L.P. All Rights Reserved.
//
//----------------------------------------------------------------------------
// $(document).on('turbolinks:load', function() {
//
//
// });


function printable(){
    doPrint(".print-kitchen-recipt");
}

function doPrint(key){
    var mode = 'iframe'; //popup
    var close = mode == "popup";
    var options = {
        mode: mode,
        popClose: close
    };
    $(key).printArea(options);
}
$(document).ready(function() {

    $("#print").click(function() {
        doPrint(".print-kitchen-recipt");
    });

    $("#print-sticker").click(function() {
        doPrint(".print-sticker-recipt");
    });

    $("#btn-print-kitchen").click(function () {
        $('#print_type').val('kitchen');
        $('#multiple-print-form').submit();
    });
    // $('#search_delivery_date').daterangepicker({ format: 'DD/MM/YYYY', autoUpdateInput: false });
    // $('#search_created_at').daterangepicker({ format: 'DD/MM/YYYY', autoUpdateInput: false });

    function initializeRangePicker(key){
        $(key).daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $(key).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        });

        $(key).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    }
    initializeRangePicker('#search_delivery_date');
    initializeRangePicker('#search_created_at');
});