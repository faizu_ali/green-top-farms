// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery-ui
//= require popper
//= require bootstrap
//= require jquery.sparkline
//= require jquery.slimscroll
//= require jquery-jvectormap-1.2.2.min
//= require jquery-jvectormap-world-mill-en
//= require jquery.flot
//= require jquery.flot.resize
//= require fastclick
//= require dashboard2
//= require demo
//= require bootstrap-datepicker
//= require toast
//= require print_area
//= require moment
//= require daterangepicker
//= require template
//= require flash
//= require dymo3
//= require custom
$.widget.bridge('uibutton', $.ui.button);